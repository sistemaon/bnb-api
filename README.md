# bnb-api

bnb-api

## Como Utilizar

#### Instalar Dependêcias
```npm
npm install
```

#### Rodar o Projeto
```
npm run dev:start
```

#### Parar Instância do Projeto
```
npm run dev:stop
```

#### Excluir Instância do Projeto
```
npm run dev:del
```

#### Parar e Excluir Instância do Projeto
```
npm run dev:stopdel
```

#### Monitorar o Projeto
```
npm run dev:monit
```

###### Requerimentos
- nodejs
- mongodb
- pm2
