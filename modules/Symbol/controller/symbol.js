
// SymbolJS

// configuration environment from configs file dev.example.js
const configDev = require('../../../configs/dev') // dev.example
const SINGLETONINCOME = require('../../../singletons/income') // dev.example

const Binance = require('node-binance-api')
const talib = require('talib')
const fs = require("fs")

const binance = new Binance().options({
    APIKEY: configDev.API_KEY,
    APISECRET: configDev.API_SECRET,

    // TOREAD: check and read about these configs, what do they do...
    recvWindow: 600000,
    reconnect: true
})


const symbolPrice = (req, res, next) => {
    const sp = req.body.sp || 'BTCEUR'
    binance.prices(sp, (error, ticker) => {
        // console.info("Price of SYMBOL/PRICE: ", ticker)
        res.status(200).json({ 'SYMBOL/PRICE': ticker })
    })
}

const bestBidAsk = (req, res, next) => {
    const sp = req.body.sp || 'BTCEUR'
    binance.websockets.depthCache([sp], (symbol, depth) => {
        let bids = binance.sortBids(depth.bids)
        let asks = binance.sortAsks(depth.asks)
        // console.info(symbol + " depth cache update")
        // console.info("bids: ", bids)
        // console.info("asks: ", asks)
        // console.info("best bid: " + binance.first(bids))
        // console.info("best ask: " + binance.first(asks))
        // console.info("last updated: " + new Date(depth.eventTime))
        res.status(200).json({ 'Best Bid: ': binance.first(bids), 'Best Ask: ': binance.first(asks), 'Last Updated: ': new Date(depth.eventTime)})
    })
}

const tradeHistory = (req, res, next) => {
  binance.trades('BTCUSDT', async (error, trades, symbol) => {
    // console.info(symbol+" trade history", trades)
    const historicalTrade = await binance.futuresHistoricalTrades('BTCUSDT')
    console.info( historicalTrade )
    const tradesInfo = symbol + " trade history."
    res.status(200).json({ 'trade_info': tradesInfo, 'trade_history': trades, 'historical_trade': historicalTrade })
  })

        // {
        //     "id": 228133332,
        //     "pricenm": "11386.43",
        //     "qty": "0.001",
        //     "quoteQty": "11.38",
        //     "time": 1602710909298,
        //     "isBuyerMaker": true
        // }
}

const orderStatus = async (req, res, next) => {
  let orderid = '226633630'
  const orderStatusFromId = await binance.futuresOrderStatus( 'BTCUSDT', { orderId: orderid } )
  res.status(200).json({ 'order_status': orderStatusFromId })
}

const historicalTrades = (req, res, next) => {
  binance.historicalTrades('BTCUSDT', (error, response) => {
     res.status(200).json({ 'hitorical_trades': response })
  })

        // {
        //     "id": 434372818,
        //     "price": "11386.99000000",
        //     "qty": "0.03477600",
        //     "quoteQty": "395.99396424",
        //     "time": 1602710945023,
        //     "isBuyerMaker": false,
        //     "isBestMatch": true
        // }
}

const futuresAllOrders = async (req, res, next) => {
  const allOrders = await binance.futuresAllOrders('BTCUSDT')
  res.status(200).json({ 'all_orders': allOrders })
}

const futuresOpenOrders = async (req, res, next) => {
  const openOrders = await binance.futuresOpenOrders('BTCUSDT')
  res.status(200).json({ 'open_orders': openOrders })
}

const promiseRequestTime = async (req, res, next) => {
  try {
    const promiseRequest = await binance.promiseRequest( 'v1/time' )
    res.status(200).json({ 'promise_request_time': promiseRequest })
  } catch (e) {
    res.status(400).json({ 'err': e })
  }
}

const futuresIncomeTotalLoss = async (req, res, next) => {
  try {
    const income = await binance.futuresIncome()
    // res.status(200).json({ 'future_income': income })

    const incomePNL = income.filter( pnl => {
      // console.info('pnl ::; ', pnl)
      return pnl.incomeType === 'REALIZED_PNL'
    })
    // res.status(200).json({ 'future_income': incomePNL })

    // const incomeProfit = incomePNL.filter( pnl => {
    //   // console.info('pnl ::; ', pnl)
    //   return pnl.income > 0
    // })
    // res.status(200).json({ 'future_income': incomeProfit })

    const incomeLoss = incomePNL.filter( pnl => {
      // console.info('pnl ::; ', pnl)
      return pnl.income < 0
    })
    // res.status(200).json({ 'future_income': incomeLoss })

    const incomeLossQuantity = incomeLoss.map( pnl => {
      // console.info('pnl ::; ', pnl)
      return pnl.income
    })
    // res.status(200).json({ 'future_income': incomeLossQuantity })

    const stringToFloat = (value) => Number.parseFloat(value)
    const floatToString = (value) => value.toString()

    const incomeLossTotal = incomeLossQuantity.reduce( (acc, cur) => {
      const accFloat = stringToFloat(acc)
      const curFloat = stringToFloat(cur)
      const totalFloat = accFloat + curFloat
      const totalString = floatToString(totalFloat)
      return totalString
    })

    res.status(200).json({ 'future_income_total_loss': incomeLossTotal })

  } catch (e) {
    res.status(400).json({ 'err': e })
  }
}

const futuresIncomeTotalGain = async (req, res, next) => {
  try {
    const income = await binance.futuresIncome()
    // res.status(200).json({ 'future_income': income })

    const incomePNL = income.filter( pnl => {
      // console.info('pnl ::; ', pnl)
      return pnl.incomeType === 'REALIZED_PNL'
    })
    // res.status(200).json({ 'future_income': incomePNL })

    // const incomeProfit = incomePNL.filter( pnl => {
    //   // console.info('pnl ::; ', pnl)
    //   return pnl.income > 0
    // })
    // res.status(200).json({ 'future_income': incomeProfit })

    const incomeGain = incomePNL.filter( pnl => {
      // console.info('pnl ::; ', pnl)
      return pnl.income > 0
    })
    // res.status(200).json({ 'future_income': incomeGain })

    const incomeGainQuantity = incomeGain.map( pnl => {
      // console.info('pnl ::; ', pnl)
      return pnl.income
    })
    // res.status(200).json({ 'future_income': incomeGainQuantity })

    const stringToFloat = (value) => Number.parseFloat(value)
    const floatToString = (value) => value.toString()

    const incomeGainTotal = incomeGainQuantity.reduce( (acc, cur) => {
      const accFloat = stringToFloat(acc)
      const curFloat = stringToFloat(cur)
      const totalFloat = accFloat + curFloat
      const totalString = floatToString(totalFloat)
      return totalString
    })

    res.status(200).json({ 'future_income_total_gain': incomeGainTotal })

  } catch (e) {
    res.status(400).json({ 'err': e })
  }
}

const futuresIncomeAll = async (req, res, next) => {
  try {
    const income = await binance.futuresIncome()

    const incomeAllCheck = income.filter( pnl => {
      // console.log(pnl)
      return pnl
    })
    console.log(incomeAllCheck)
    const incomeAllFrom = incomeAllCheck.map( pnl => {
      ////// REFACT
      const pnlTimeDate = new Date(pnl.time)
      const pnlTimeDateDay = pnlTimeDate.getDate().toString()
      const pnlTimeDateMonth = (pnlTimeDate.getMonth() + 1).toString()
      const pnlTimeDateYear = pnlTimeDate.getFullYear().toString()
      const pnlTimeDateAll = `${pnlTimeDateDay}-${pnlTimeDateMonth}-${pnlTimeDateYear}`
      ////// REFACT
      const incomeFormat = {
        'income': pnl.income,
        'date': pnlTimeDateAll
      }
      return incomeFormat
    })
    // console.log(incomeAllFrom)
    SINGLETONINCOME.all = incomeAllFrom
    res.status(200).json({ 'future_income_all': SINGLETONINCOME })

  } catch (e) {
    res.status(400).json({ 'err': e })
  }
}

const futuresIncomeAllByDate = async (req, res, next) => {
  try {
    const income = await binance.futuresIncome()
    const incomeReqParams = req.params
    const dateIncome = incomeReqParams.date

    const incomeAllDateCheck = income.filter( pnl => {
      ////// REFACT
      const pnlTimeDate = new Date(pnl.time)
      const pnlTimeDateDay = pnlTimeDate.getDate().toString()
      const pnlTimeDateMonth = (pnlTimeDate.getMonth() + 1).toString()
      const pnlTimeDateYear = pnlTimeDate.getFullYear().toString()
      const pnlTimeDateAll = `${pnlTimeDateDay}-${pnlTimeDateMonth}-${pnlTimeDateYear}`
      ////// REFACT
      return pnlTimeDateAll === dateIncome
    })
    const incomeAllFromDate = incomeAllDateCheck.map( pnl => {
      ////// REFACT
      const pnlTimeDate = new Date(pnl.time)
      const pnlTimeDateDay = pnlTimeDate.getDate().toString()
      const pnlTimeDateMonth = (pnlTimeDate.getMonth() + 1).toString()
      const pnlTimeDateYear = pnlTimeDate.getFullYear().toString()
      const pnlTimeDateAll = `${pnlTimeDateDay}-${pnlTimeDateMonth}-${pnlTimeDateYear}`
      ////// REFACT
      const incomeDateFormat = {
        'income': pnl.income,
        // 'date': pnlTimeDate,
        // 'dayOfMonth': pnlTimeDateDay,
        // 'month': pnlTimeDateMonth,
        // 'year': pnlTimeDateYear,
        'date': pnlTimeDateAll
      }
      return incomeDateFormat
    })

    SINGLETONINCOME.all = incomeAllFromDate
    res.status(200).json({ 'future_income_all': SINGLETONINCOME, 'date_check': dateIncome })

  } catch (e) {
    res.status(400).json({ 'err': e })
  }
}

const futuresFundingRate = async (req, res, next) => {
  try {
    const fundingRate = await binance.futuresFundingRate()
    res.status(200).json({ 'funding_rate': fundingRate })
  } catch (e) {
    res.status(400).json({ 'err': e })
  }
}

const talibIndicator = async (req, res, next) => {
  try {
    // Load market data
    const marketContents = fs.readFileSync('talibDataTest.json','utf8')
    const marketData = JSON.parse(marketContents)
    // load the module and display its version

    // execute SMA indicator function with time period 180
    talib.execute({
        name: "RSI",
        startIdx: 0,
        endIdx: marketData.close.length - 1,
        inReal: marketData.close,
        optInTimePeriod: 180
    }, function (err, result) {
        // Show the result array
        console.log("SMA Function Results:")
        console.log(result)
        res.status(200).json({ 'indicator_rsi': result })
    })

    // console.log("TALib Version: " + talib.version)
    // // Display all available indicator function names
    // var functions = talib.functions
    // for (i in functions) {
    //   console.log(functions[i].name)
    //   // res.status(200).json({ 'funding_rate': functions[i].name })
    // }
  } catch (e) {
    res.status(400).json({ 'err': e })
  }

}

const symbolControllers = {
    symbolPrice,
    bestBidAsk,
    tradeHistory,
    orderStatus,
    historicalTrades,
    futuresAllOrders,
    futuresOpenOrders,
    promiseRequestTime,
    futuresIncomeTotalLoss,
    futuresIncomeTotalGain,
    futuresIncomeAll,
    futuresIncomeAllByDate,
    futuresFundingRate,

    talibIndicator
}

module.exports = symbolControllers
