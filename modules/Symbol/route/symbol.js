
// SymbolJS

const express = require('express')
const router = express.Router()

const symbolControllers = require('../controller/symbol')

router.post('/symbol-price', symbolControllers.symbolPrice)
router.post('/symbol-best', symbolControllers.bestBidAsk)
router.post('/trade-history', symbolControllers.tradeHistory)
router.post('/order-status', symbolControllers.orderStatus)
router.post('/historical-trade', symbolControllers.historicalTrades)
router.post('/all-orders', symbolControllers.futuresAllOrders)
router.post('/open-orders', symbolControllers.futuresOpenOrders)
router.post('/promise-request-time', symbolControllers.promiseRequestTime)
router.post('/future-income/total-loss', symbolControllers.futuresIncomeTotalLoss)
router.post('/future-income/total-gain', symbolControllers.futuresIncomeTotalGain)
router.get('/future-income/total-all', symbolControllers.futuresIncomeAll)
router.get('/future-income/total-all/date/:date', symbolControllers.futuresIncomeAllByDate)
router.post('/funding-rate', symbolControllers.futuresFundingRate)
router.post('/talib', symbolControllers.talibIndicator)

module.exports = router
