// SymbolJS

// Mongoose to save on database

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SymbolSchema = new Schema({
    // symbol info
      symbol: { type: String, required: true, unique: true },
      price: { type: String, required: true, unique: true }
})

// mongoose user model mongo
const Symbol = mongoose.model('Symbol', SymbolSchema)

module.exports = Symbol